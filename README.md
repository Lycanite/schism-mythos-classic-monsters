# Schism Mythos Classic Monsters

This Doom mod is an addon for the `schism-mythos` mod, it adds classic Doom, Heretic, Hexen and Blood monsters to its random monster spawner.