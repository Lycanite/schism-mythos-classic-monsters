class SchismMonsterSpawns : StaticEventHandler {
    SchismCreatureSpawns spawns;

	virtual SchismCreatureSpawns GetSpawns() {
		if (!self.spawns) {
			self.spawns = SchismCreatureSpawns.Get();
			self.PopulateSpawns(self.spawns);
		}
		return self.spawns;
	}

    /**
     * Called on event handler registration.
     */
    override void OnRegister() {
		self.SetOrder(100);
    }

    /**
     * Called during actor class replacement.
     * @param event.replacee The actor class being replaced.
     * @param event.replacement The replacement actor class, can be edited.
     * @param event.isFinal If true, another event handler has set a replacement already, should probably not be changed further.
     */
    override void CheckReplacement(ReplaceEvent event) {
        if (event.isFinal) {
            return;
        }
        self.GetSpawns();
    }

    /**
     * Called when checking replacement actors via other functions (A_BossDeath), etc. Should match CheckReplacement.
     * @param event The actor replacement event. Replacement and replacee are reversed compared to CheckReplacement.
     */
    override void CheckReplacee(ReplacedEvent event) {
        if (event.isFinal) {
            return;
        }
        self.GetSpawns();
    }

	/**
	 * Populates entries on the creature spawner, called each time a new creature spawner thinker is created.
	 */
	virtual void PopulateSpawns(SchismCreatureSpawns creatureSpawns) {
		self.PopulateBloodMonsterRoles(creatureSpawns);
		
		Cvar fullReplacementCvar = Cvar.FindCvar("schism_spawners_full_replace");
		bool fullReplacement = fullReplacementCvar.GetBool();
		if (!fullReplacement) { // If false, classic monsters have spawn entries added, including them in the random spawn lists.
			self.PopulateClassicSpawns(creatureSpawns);
		}
	}

	/**
	 * Populates the roles of blood monsters (included in this mod).
	 */
	virtual void PopulateBloodMonsterRoles(SchismCreatureSpawns creatureSpawns) {
		creatureSpawns.AddRoleMapping("CultistAcolyte", SPAWN_ROLE_GRUNT);
		creatureSpawns.AddRoleMapping("CultistAngry", SPAWN_ROLE_BOUNCER);
		creatureSpawns.AddRoleMapping("CultistFanatic", SPAWN_ROLE_SNIPER);
		creatureSpawns.AddRoleMapping("BloatedButcher", SPAWN_ROLE_LOBBER);
		creatureSpawns.AddRoleMapping("AxeZombie", SPAWN_ROLE_BRUTE);
		creatureSpawns.AddRoleMapping("HellHound", SPAWN_ROLE_BRUTE);
		creatureSpawns.AddRoleMapping("BuriedZombie", SPAWN_ROLE_ASSASSIN);
		creatureSpawns.AddRoleMapping("GillBeast", SPAWN_ROLE_ASSASSIN);
		creatureSpawns.AddRoleMapping("Phantasm", SPAWN_ROLE_BROOD);
		creatureSpawns.AddRoleMapping("CultistPriest", SPAWN_ROLE_SEEKER);
		creatureSpawns.AddRoleMapping("CultistZealot", SPAWN_ROLE_CANNON);
	}

	/**
	 * Populates classic (and blood) monster spawn entries on the creature spawner allowing them to randomly spawn alongside other monsters in the same role.
	 */
	virtual void PopulateClassicSpawns(SchismCreatureSpawns creatureSpawns) {
		// Doom:
		creatureSpawns.AddSpawn("ZombieMan", SPAWN_ROLE_GRUNT, "doom", true);
		creatureSpawns.AddSpawn("ShotgunGuy", SPAWN_ROLE_BOUNCER, "doom", true);
		creatureSpawns.AddSpawn("ChaingunGuy", SPAWN_ROLE_SNIPER, "doom", true);
		creatureSpawns.AddSpawn("DoomImp", SPAWN_ROLE_SLINGER, "doom", true);
		creatureSpawns.AddSpawn("HellKnight", SPAWN_ROLE_LOBBER, "doom", true);
		creatureSpawns.AddSpawn("BaronOfHell", SPAWN_ROLE_BARON, "doom", true);
		creatureSpawns.AddSpawn("Demon", SPAWN_ROLE_BRUTE, "doom", true);
		creatureSpawns.AddRoleMapping("Spectre", SPAWN_ROLE_ASSASSIN);
		creatureSpawns.AddSpawn("LostSoul", SPAWN_ROLE_SOUL, "doom", true);
		creatureSpawns.AddSpawn("Cacodemon", SPAWN_ROLE_GHAST, "doom", true);
		creatureSpawns.AddSpawn("PainElemental", SPAWN_ROLE_BROOD, "doom", true);
		creatureSpawns.AddSpawn("Arachnotron", SPAWN_ROLE_TANK, "doom", true);
		creatureSpawns.AddSpawn("Revenant", SPAWN_ROLE_SEEKER, "doom", true);
		creatureSpawns.AddSpawn("Archvile", SPAWN_ROLE_CASTER, "doom", true);
		creatureSpawns.AddSpawn("Fatso", SPAWN_ROLE_CANNON, "doom", true);
		creatureSpawns.AddSpawn("Cyberdemon", SPAWN_ROLE_DESTROYER, "doom", true);
		creatureSpawns.AddSpawn("SpiderMastermind", SPAWN_ROLE_MASTERMIND, "doom", true);

		// Heretic:
		creatureSpawns.AddSpawn("Mummy", SPAWN_ROLE_GRUNT, "heretic", true);
		creatureSpawns.AddSpawn("MummyLeader", SPAWN_ROLE_BOUNCER, "heretic", true);
		creatureSpawns.AddSpawn("Clink", SPAWN_ROLE_SNIPER, "heretic", true);
		creatureSpawns.AddSpawn("Knight", SPAWN_ROLE_SLINGER, "heretic", true);
		creatureSpawns.AddSpawn("Ironlich", SPAWN_ROLE_BARON, "heretic", true);
		creatureSpawns.AddSpawn("Beast", SPAWN_ROLE_BRUTE, "heretic", true);
		creatureSpawns.AddSpawn("MummyGhost", SPAWN_ROLE_ASSASSIN, "heretic", true);
		creatureSpawns.AddSpawn("HereticImp", SPAWN_ROLE_SOUL, "heretic", true);
		creatureSpawns.AddSpawn("Wizard", SPAWN_ROLE_GHAST, "heretic", true);
		creatureSpawns.AddSpawn("Snake", SPAWN_ROLE_SEEKER, "heretic", true);

		// Hexen:
		creatureSpawns.AddSpawn("Ettin", SPAWN_ROLE_BOUNCER, "hexen", true);
		creatureSpawns.AddSpawn("Demon1", SPAWN_ROLE_SLINGER, "hexen", true);
		creatureSpawns.AddSpawn("Demon2", SPAWN_ROLE_LOBBER, "hexen", true);
		creatureSpawns.AddSpawn("Centaur", SPAWN_ROLE_BRUTE, "hexen", true);
		creatureSpawns.AddSpawn("Stalker", SPAWN_ROLE_ASSASSIN, "hexen", true);
		creatureSpawns.AddSpawn("Afrit", SPAWN_ROLE_SOUL, "hexen", true);
		creatureSpawns.AddSpawn("Wraith", SPAWN_ROLE_GHAST, "hexen", true);
		creatureSpawns.AddSpawn("Bishop", SPAWN_ROLE_BROOD, "hexen", true);
		creatureSpawns.AddSpawn("CentaurLeader", SPAWN_ROLE_SEEKER, "hexen", true);
		creatureSpawns.AddSpawn("Iceguy", SPAWN_ROLE_CANNON, "hexen", true);

		// Blood:
		creatureSpawns.AddSpawn("CultistAcolyte", SPAWN_ROLE_GRUNT, "blood", true);
		creatureSpawns.AddSpawn("CultistAngry", SPAWN_ROLE_BOUNCER, "blood", true);
		creatureSpawns.AddSpawn("CultistFanatic", SPAWN_ROLE_SNIPER, "blood", true);
		creatureSpawns.AddSpawn("BloatedButcher", SPAWN_ROLE_LOBBER, "blood", true);
		creatureSpawns.AddSpawn("AxeZombie", SPAWN_ROLE_BRUTE, "blood", true);
		creatureSpawns.AddSpawn("HellHound", SPAWN_ROLE_BRUTE, "blood", true);
		creatureSpawns.AddSpawn("BuriedZombie", SPAWN_ROLE_ASSASSIN, "blood", true);
		creatureSpawns.AddSpawn("GillBeast", SPAWN_ROLE_ASSASSIN, "blood", true);
		creatureSpawns.AddSpawn("Phantasm", SPAWN_ROLE_BROOD, "blood", true);
		creatureSpawns.AddSpawn("CultistPriest", SPAWN_ROLE_SEEKER, "blood", true);
		creatureSpawns.AddSpawn("CultistZealot", SPAWN_ROLE_CANNON, "blood", true);
	}
}